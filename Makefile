
VERSION = 0.2.0
RELEASE_DIR = ../02-isomaze-release
 
# "HTML5"
# "Windows Desktop"
# "Android"
# "Mac OSX"
# "Linux X11"
# "BlackBerry 10"

release: linux windows mac html5
	echo ${VERSION} is ready!


linux:
	mkdir ${RELEASE_DIR}/isomaze-${VERSION}
	godot -export "Linux X11" ${RELEASE_DIR}/isomaze-${VERSION}/isomaze-${VERSION}.x86_64
	chmod a+x ${RELEASE_DIR}/isomaze-${VERSION}/isomaze-${VERSION}.x86_64
	cd ${RELEASE_DIR} && chmod a+x isomaze-${VERSION}/isomaze-${VERSION}.x86_64
	cd ${RELEASE_DIR} && tar czfv isomaze-${VERSION}.x86_64.tar.gz isomaze-${VERSION}
	cd ${RELEASE_DIR} && rm -rv isomaze-${VERSION}

windows:
	godot -export "Windows Desktop" ${RELEASE_DIR}/isomaze-${VERSION}.exe
	cd ${RELEASE_DIR} && zip -r isomaze-${VERSION}.win32.zip isomaze-${VERSION}.exe data.pck
	cd ${RELEASE_DIR} && rm -v ${RELEASE_DIR}/isomaze-${VERSION}.exe data.pck

mac:
	godot -export "Mac OSX" ${RELEASE_DIR}/isomaze-${VERSION}.x86_64.pkg

html5:
	mkdir ${RELEASE_DIR}/isomaze-${VERSION}-html5
	godot -export "HTML5" ${RELEASE_DIR}/isomaze-${VERSION}-html5/isomaze.html
	cd ${RELEASE_DIR}/isomaze-${VERSION}-html5 && ln -s isomaze.html index.html
