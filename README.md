# Isomaze

Isometric-Maze game developed with [Godot](https://godotengine.org/).

You are stuck with your husband in a dungerous maze. Will you find your husband?

![Isomaze](https://i.imgur.com/NOY1gBg.jpg)

## Download (v0.2.0)

* [Linux (x86_64)](https://gitlab.com/shangrila/isomaze/uploads/7f3a1e14b77b58b5ea28b01389609ba5/isomaze-0.2.0.x86_64.tar.gz)
* [Windows](https://gitlab.com/shangrila/isomaze/uploads/21afc3b7528fc33185bc885e3a738c48/isomaze-0.2.0.win32.zip)
* [MacOS](https://mega.nz/#!UoBQCbpB!zkRUqMebYeHfJjeVmf-E6a1MwyKJUlNxOekpWH9A6b8)
* [HTML5](https://shangrila.gitlab.io/isomaze/)


### Credits and License

* Game by [Onur Aslan](https://onur.im/) (GPLv2)
* Isometric Dungeon Tiles by [Kenney](http://kenney.nl/assets/isometric-dungeon-tiles) (CC0)
* Shagra the Orc, Steward and Shambler by [Pioneer Valley Games](http://www.pioneervalleygames.com/free-resources.html) (CC0)
* Fireball Spell by [Clint Bellanger](http://opengameart.org/content/fireball-spell) (CC-BY 3.0)
