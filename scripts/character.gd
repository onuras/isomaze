extends KinematicBody2D

var MOTION_SPEED = 360

var _timer = null
var score = 0
var anim = null
var fireball_speed = 5

func _ready():
	set_fixed_process(true)
	set_process_input(true)

	score = 0

	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_every_second")
	_timer.set_wait_time(1.0)
	_timer.set_one_shot(false)
	_timer.start()

func _input(event):
	if (event.is_action_released("shoot")):
		_shoot()


func _fixed_process(delta):
	var motion = Vector2()

	if (Input.is_action_pressed("ui_up")):
		motion += Vector2(1.7, -1)
		_run_anim("RunNorth")
	elif (Input.is_action_pressed("ui_down")):
		motion += Vector2(-1.7, 1)
		_run_anim("RunSouth")
	elif (Input.is_action_pressed("ui_left")):
		motion += Vector2(-1.7, -1)
		_run_anim("RunWest")
	elif (Input.is_action_pressed("ui_right")):
		motion += Vector2(1.7, 1)
		_run_anim("RunEast")
	else:
		_stop_anim()

	motion = motion.normalized()*MOTION_SPEED*delta
	motion = move(motion)

	# check collision
	if is_colliding():
		var collider = get_collider()
		if collider.get_name() == "walls":
			var colliding = get_collider_metadata()
			if collider.get_cell(colliding[0], colliding[1]) == 10:
				_return_to_main_menu("You found your husband in " + String(score) + " seconds!")
		# Check if character colliding with a shambler
		elif "enemies" in collider.get_groups():
			_return_to_main_menu("A shambler got you! And you died!")


func _return_to_main_menu(text):
	get_node('../../').queue_free()
	var s = ResourceLoader.load('res://scenes/main_menu.tscn')
	var instance = s.instance()
	var label = instance.get_node("Label")
	label.set_text(text)
	label.set_hidden(false)
	get_node("/root").add_child(instance)


func _shoot():
	var s = ResourceLoader.load('res://scenes/fireball.tscn')
	var fireball = s.instance()

	var pos = get_pos()

	if (anim == null or anim == "RunEast" or anim == "IdleEast"):
		pos += Vector2(10, 12)
		fireball.direction = "East"
	elif (anim == "RunWest" or anim == "IdleWest"):
		pos += Vector2(-45, -25)
		fireball.direction = "West"
	elif (anim == "RunSouth" or anim == "IdleSouth"):
		pos += Vector2(-42,  10)
		fireball.direction = "South"
	elif (anim == "RunNorth" or anim == "IdleNorth"):
		pos += Vector2(12,  -10)
		fireball.direction = "North"

	fireball.MOTION_SPEED = MOTION_SPEED * 2
	fireball.set_pos(pos)
	get_node('/root/Maze/walls').add_child(fireball)

func _run_anim(name):
	if anim == name:
		return

	var player = get_node("Sprite/AnimationPlayer")
	player.play(name)
	anim = name

	# hide objective
	get_node("Node2D/objective").set_hidden(true)

func _stop_anim():
	if anim == "RunEast":
		_run_anim("IdleEast")
	elif anim == "RunWest":
		_run_anim("IdleWest")
	elif anim == "RunSouth":
		_run_anim("IdleSouth")
	elif anim == "RunNorth":
		_run_anim("IdleNorth")


func _every_second():
	score += 1
	get_node("Node2D/Label").set_text(String(score))