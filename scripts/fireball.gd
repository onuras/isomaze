extends KinematicBody2D

var MOTION_SPEED = 720
var direction = null

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	if direction == null:
		return


	var motion = Vector2()

	if direction == "East":
		motion += Vector2(1.7, 1)
		set_rot(-0.5)
	elif direction == "West":
		motion += Vector2(-1.7, -1)
		set_rot(2.5)
	elif direction == "North":
		motion += Vector2(1.7, -1)
		set_rot(0.5)
	elif direction == "South":
		motion += Vector2(-1.7, 1)
		set_rot(3.7)

	motion = motion.normalized()*MOTION_SPEED*delta
	motion = move(motion)

	if is_colliding():
		var collider = get_collider()
		if collider.get_name().find("Shambler") != -1:
			collider.HP -= randi() % 50
			if collider.HP <= 0:
				collider.queue_free()
		queue_free()

func _on_VisibilityNotifier2D_exit_screen():
	queue_free()
