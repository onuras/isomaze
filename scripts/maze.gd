extends Node2D

var is_zoomed = false
var zooming = false

func _ready():
	set_process_input(true)
	#var Z = maze(10, 10)
	#_print_maze(Z)
	#draw_maze(Z)
	print("READY")

func _input(event):
	var body = get_node("walls/Character")
	if (event.is_action_released("resize") and not is_zoomed):
		is_zoomed = true
		scale(Vector2(0.25, 0.25))
		body.MOTION_SPEED *= 0.25
	elif (event.is_action_released("resize") and is_zoomed):
		is_zoomed = false
		scale(Vector2(4, 4))
		body.MOTION_SPEED *= 4
	# Speedup and down
	elif (event.is_action_released("speed_up")):
		body.MOTION_SPEED *= 2
	elif (event.is_action_released("speed_down")):
		body.MOTION_SPEED /= 2

func maze(width=81, height=51, complexity=.75, density=.75):
	# randommize seed
	randomize()

	var shape = [ (height / 2) * 2 + 1, (width / 2) * 2 + 1 ]

	# normalize complexity and density
	complexity = int(complexity * (5 * (shape[0] + shape[1])))
	density = int(density * ((shape[0] / 2) * (shape[1] / 2)))

	# create empty maze
	var Z = []
	for i in range(height):
		var row = []
		for j in range(width): row.append(false)
		Z.append(row)

	# fill borders
	for i in range(width):
		Z[0][i] = true
		Z[-1][i] = true

	for i in Z:
		i[0] = true
		i[-1] = true

	# Now make actual maze
	for i in range(density):
		var x = int(randi() % (shape[1] / 2)) * 2 - 1
		var y = int(randi() % (shape[0] / 2)) * 2
		Z[y][x] = true

		for j in range(complexity):
			var neighbours = []
			if x > 1:            neighbours.append([y, x - 2])
			if x < shape[1] - 2: neighbours.append([y, x + 2])
			if y > 1:            neighbours.append([y - 2, x])
			if y < shape[0] - 2: neighbours.append([y + 2, x])

			if not neighbours.empty():
				var random_neighbour = int(randi() % (neighbours.size() - 1))
				var y_ = neighbours[random_neighbour][0]
				var x_ = neighbours[random_neighbour][1]
				if not Z[y_][x_]:
					Z[y_][x_] = true
					Z[y_ + (y - y_) / 2][x_ + (x - x_) / 2] = true
					x = x_
					y = y_

	return Z


func draw_maze(maze):
	var maze_height = maze.size()
	var maze_width = maze[0].size()
	# lets say we have a maze[30][30]
	# every maze will use 4 tile from our tilesets
	# for example
	# maze[0][0] = (0, 0), (1, 0)
	#              (0, 1), (1, 1)
	# maze[0][1] = (2, 0), (3, 0)
	#              (2, 1), (3, 1)
	# maze[1][1] = (2, 2), (3, 2)
	#              (2, 3), (3, 3)
	var base_map = get_node("base")
	var walls_map = get_node("walls")
	var free_cells = []
	for width in range(maze_width):
		for height in range(maze_height):
			var base_tile = 0
			if randi() % 10 == 0:
				base_tile = 1
			# fill base
			base_map.set_cell(width * 2, height * 2, 0)
			base_map.set_cell(width * 2 + 1, height * 2, 0)
			base_map.set_cell(width * 2, height * 2 + 1, 0)
			base_map.set_cell(width * 2 + 1, height * 2 + 1, base_tile)

			if maze[height][width]:
				var x = width * 2
				var y = height * 2
				# north (0, 0)
				# west (1, 0)
				walls_map.set_cell(x, y, 7)
				walls_map.set_cell(x + 1, y, 6)
				walls_map.set_cell(x, y + 1, 9)
				walls_map.set_cell(x + 1, y + 1, 8)
			else:
				free_cells.append([width * 2, height * 2])
	# set position of steward
	var cell = free_cells[randi() % free_cells.size() - 1]
	walls_map.set_cell(cell[0], cell[1], 10)

	_add_enemies(free_cells)


func _add_enemies(free_cells):
	var enemy_res = ResourceLoader.load('res://scenes/shambler.tscn')
	var walls = get_node("walls")
	var cell_size = walls.get_cell_size()
	for cell in free_cells:
		# Always skip first cells, we don't want enemies to stuck our character
		if cell[1] == 2 and cell[0] < 5:
			continue

		# randomize enemies
		if randi() % 3 != 0:
			continue

		var enemy_pos = _get_tilemap_coord(walls, cell[0], cell[1])
		var instance = enemy_res.instance()
		instance.set_pos(enemy_pos)
		walls.add_child(instance)


func _get_tilemap_coord(tilemap, x, y):
	var offset = Vector2(50, 50)       # This is my game specific offset
	var cell_size = tilemap.get_cell_size()
	var pos = Vector2()
	var row = x + y
	var col = x - y
	pos.y = row * (cell_size.y / 2) + offset.x
	pos.x = col * (cell_size.x / 2) + offset.y
	return pos




func _print_maze(maze):
	for i in maze:
		var arr = ""
		for j in i:
			if j:
				arr += '1'
			else:
				arr += '0'
		print(String(arr))