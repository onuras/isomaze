extends KinematicBody2D

var MOTION_SPEED = 0
var HP = 120
var _anim = null

func _ready():
	MOTION_SPEED = floor(rand_range(100, 300))
	set_fixed_process(true)
	add_to_group("enemies")

func _fixed_process(delta):
	var pos = get_pos()
	var character_pos = get_node("../Character").get_pos()
	var dist = character_pos.distance_to(pos)
	if dist < 600:
		var angle = pos.angle_to_point(character_pos)
		if angle > 0.5 and angle < 1.5:
			_run_anim("RunWest")
		elif angle >= 1.5 and angle < 3:
			_run_anim("RunSouth")
		elif angle >= -3 and angle < -1.5:
			_run_anim("RunEast")
		elif angle <= 0 and angle > -1.5:
			_run_anim("RunNorth")

		var direction = character_pos - pos
		move(direction.normalized() * MOTION_SPEED * delta)
	else:
		_stop_anim()


func _run_anim(anim):
	if _anim == null or _anim != anim:
		_anim = anim
		get_node("Sprite/AnimationPlayer").play(anim)

func _stop_anim():
	var sprite = get_node("Sprite")
	var anim_player = get_node("Sprite/AnimationPlayer")
	if anim_player.is_playing():
		anim_player.stop(true)
		_anim = null
	if _anim == "RunEast":
		sprite.set_frame(7)
	elif _anim == "RunWest":
		sprite.set_frame(3)
	elif _anim == "RunNorth":
		sprite.set_frame(9)
	elif _anim == "RunSouth":
		sprite.set_frame(1)
